

/**
 * Created by Roma on 03.04.2018.
 */

public class Time {
    private long timeAl1E1;
    private long timeAl1E2;
    private long timeAl2E1;
    private long timeAl2E2;

    public Time(long timeAl1E1,long timeAl1E2,long timeAl2E1, long timeAl2E2){
        this.timeAl1E1=timeAl1E1;
        this.timeAl1E2=timeAl1E2;
        this.timeAl2E1=timeAl2E1;
        this.timeAl2E2=timeAl2E2;
    }
    public long getTimeFromExp1Alg1(){return timeAl1E1;}
    public long getTimeFromExp2Alg1(){return timeAl1E2;}
    public long getTimeFromExp1Alg2(){return timeAl2E1;}
    public long getTimeFromExp2Alg2(){return timeAl2E2;}
}

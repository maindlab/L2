



import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;

/**
 * Created by Roma on 03.04.2018.
 */

public class Algos {
    public static List<List<Long>>createWayMatrix(int dotsCount,int q, int r,int m){
        List<List<Long>> myMatrix = new ArrayList<>(dotsCount);
        for (int i=0;i<dotsCount;i++){
            myMatrix.add(new ArrayList<Long>(dotsCount));
        }
        for (int i = 0; i < dotsCount; i++) {
            for (int j = 0; j <dotsCount ; j++) {
                myMatrix.get(i).add(null);
            }
        }
        for (int i = 1; i < dotsCount; i++) {
                    myMatrix.get(0).set(i,(long) (new Random().nextInt(r-q) +q));
            }
        for (int i = 0; i < dotsCount; i++) {
            for (int j = 0; j <dotsCount ; j++) {
              if(m==0)break;
                if(i!=j &&  (new Random().nextInt(10))>8){
                    m--;
                    myMatrix.get(i).set(j,(long) (new Random().nextInt(r-q) +q));
                }
            }
        }
        return myMatrix;
    }
    public static  long getTimefromDijkstra(List<List<Long>> matrix){
        class Dot{
            public boolean stat;
            public long weight;
            public Dot(boolean stat,long weight){
                this.stat=stat;
                this.weight=weight;
            }
        }
        List<Dot> dots = new ArrayList<>();
        for (int i = 0; i < matrix.size(); i++) {
            dots.add(new Dot(false,-1));
        }

        //algorithm start
        dots.get(0).weight=0;
      long now= System.currentTimeMillis();
        int z=5;
        for(int j=0;j<dots.size();j++) {
            long w=-1;
            int numbOfDot=-1;
            for (int i = 0; i < dots.size(); i++) {
                if ((w == -1 && dots.get(i).weight != -1 && !dots.get(i).stat) || (w > dots.get(i).weight && !dots.get(i).stat && dots.get(i).weight != -1)) {
                    w = dots.get(i).weight;
                    numbOfDot = i;
                }
            }
            dots.get(numbOfDot).stat=true;
            for (int i = 0; i <matrix.size() ; i++) {
               Long way= matrix.get(numbOfDot).get(i);
                if(way!=null){
                    if(dots.get(i).weight==-1)
                        dots.get(i).weight=way;
                    else if(dots.get(i).weight>dots.get(numbOfDot).weight+way)
                        dots.get(i).weight=dots.get(numbOfDot).weight+way;
                }
            }

        }


        return System.currentTimeMillis()-now;
    }

    public static long getTimefromFord(List<List<Long>> matrix){
        class ListGraph{
            public ListGraph(int A,int B,long weight){
                this.A=A;
                this.B=B;
                this.weingt=weight;
            }
            public int A;
            public int B;
            public long weingt;
        }

        List<ListGraph> myListMatrix = new ArrayList<>();
        for (int i = 0; i <matrix.size() ; i++) {
            for (int j = 0; j <matrix.size() ; j++) {
                if(matrix.get(i).get(j)!=null)
                {
                    myListMatrix.add(new ListGraph(i,j,matrix.get(i).get(j)));
                }
            }
        }
        List<Long> F = new ArrayList<>();
        for (int i = 0; i < matrix.size(); i++) {
            F.add(-1L);
        }

        F.set(0,0L);
        long now =System.currentTimeMillis();
        for (int k = 1; k < matrix.size(); ++k) {
            for (int i = 0; i < myListMatrix.size(); i++)
            {
                int start = myListMatrix.get(i).A;
                int finish = myListMatrix.get(i).B;
                long weight = myListMatrix.get(i).weingt;
                if(F.get(start)!=-1L)
                if (F.get(start) + weight < F.get(finish))
                    F.set(finish, F.get(start) + weight);
            }
        }
        F=null;
        myListMatrix=null;
        return System.currentTimeMillis()-now;
    }
}

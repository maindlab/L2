import java.util.List;

public class Program {
    public  static long a1,a2,a3,a4;
    public static int I;
    private static List<List<Long>> matrix1,matrix2;
    public static void main(String[] args) {
        DataTime.clearData();
        for (int i = 100; i <= 10000; i+=100) {
            long now = System.currentTimeMillis();
            I=i;
            Thread myM1= new Thread(new Runnable() {
                @Override
                public void run() {
                    // почему он видит ссылку на matrix, а на Long a1 не видит?
                    matrix1=Algos.createWayMatrix(I,1,1000000,I*I/10);
                }
            });
            Thread myM2= new Thread(new Runnable() {
                @Override
                public void run() {
                    // почему он видит ссылку на matrix, а на Long a1 не видит?
                    matrix2=Algos.createWayMatrix(I,1,1000000,I*I);
                }
            });
            myM1.setDaemon(true);
            myM2.setDaemon(true);
            myM1.start();
            myM2.start();
            try {
                myM1.join();
                myM2.join();
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }
            Thread myDi1= new Thread(new Runnable() {
                @Override
                public void run() {
                    // почему он видит ссылку на matrix, а на Long a1 не видит?
                  a1 = Algos.getTimefromDijkstra(matrix1);
                                    }
            });
            Thread myF1= new Thread(new Runnable() {
                @Override
                public void run() {
                    // почему он видит ссылку на matrix, а на Long a1 не видит?
                    a3 =  Algos.getTimefromFord(matrix1);
                }
            });
            Thread myDi2= new Thread(new Runnable() {
                @Override
                public void run() {
                    // почему он видит ссылку на matrix, а на Long a1 не видит?
                    a2 =  Algos.getTimefromDijkstra(matrix2);
                }
            });
            Thread myF2= new Thread(new Runnable() {
                @Override
                public void run() {
                    // почему он видит ссылку на matrix, а на Long a1 не видит?
                    a4 =  Algos.getTimefromFord(matrix2);
                }
            });
            myF1.setDaemon(true);
            myF2.setDaemon(true);
            myDi1.setDaemon(true);
            myDi2.setDaemon(true);
            myF1.start();
            myF2.start();
            myDi1.start();
            myDi2.start();
            try {
                myF2.join();
                myF1.join();
                myDi1.join();
                myDi2.join();
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }
            matrix1=null;
            matrix2=null;
            DataTime.setNewData(new Time(a1,a2,a3,a4));
            System.out.print(i);
            System.out.print(" ");
            System.out.print(System.currentTimeMillis()-now);
            System.out.print(" ");
            System.out.print(a1);
            System.out.print(" ");
            System.out.print(a2);
            System.out.print(" ");
            System.out.print(a3);
            System.out.print(" ");
            System.out.println(a4);
            System.gc();
        }
        DataTime.clearData();
    }
}



import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roma on 03.04.2018.
 */

public class DataTime {
    private static List<Time> myData ;
    public static void setNewData(Time time){
        myData.add(time);
    }
    public static void clearData(){
        myData=new ArrayList<>();
    }
    public  static List<Time> getMyData(){
        return new ArrayList<>(myData);
    }
}
